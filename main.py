import sqlite3
conn = sqlite3.connect('employee.db')
c = conn.cursor()

c.execute("""CREATE TABLE employee (
            name text,
            id integer,
            salary integer,
            department_id integer PRIMARY KEY
         )""")

c.execute("""ALTER TABLE employee
           ADD city text""")

employee_values = "INSERT INTO employee (name, id, salary, department_id, city) VALUES (?, ?, ?, ?, ?)"
c.execute(employee_values, ('Jack', 5 , 20000, 1,'manhattan'))
c.execute(employee_values, ('Hector', 6, 30000, 2, 'seattle'))
c.execute(employee_values, ('Jill', 7, 35000, 3, 'trivandrum'))
c.execute(employee_values, ('Margret', 8, 45000, 4, 'kochi'))
c.execute(employee_values, ('Sammy', 9, 40000, 5, 'L.A'))

conn.commit()

#c.execute("""SELECT name,id,salary FROM employee""")
#print(c.fetchall())

letter=input("enter the first letter of name:").upper()
c.execute("SELECT name,id,city FROM employee WHERE name LIKE '{}%'" .format(letter))
print(c.fetchall())

id = int(input("enter the id"))
c.execute("SELECT name,salary,department_id,city FROM employee WHERE department_id= ?", (id))
print(c.fetchone())

id = input('enter the id to change name')
name_to_set = input("enter new name")
c.execute("UPDATE employee SET name = :name_to_set WHERE id =:id", (name_to_set,id))

conn.commit()

c.execute("""CREATE TABLE departments(
          departments_id integer,
          department_name text,
          FOREIGN KEY (departments_id) REFERENCES employee (department_id)
          )""")

department_values = "INSERT INTO departments (departments_id,department_name) VALUES (?, ?)"
c.execute(department_values, (5, 'research'))
c.execute(department_values, (4, 'development'))
c.execute(department_values, (3, 'merger'))
c.execute(department_values, (2, 'accounts'))
c.execute(department_values, (1, 'HR'))

conn.commit()

c.execute("""SELECT * FROM departments""")
print(c.fetchall())

dept_id = input("enter department id to know about employee")
c.execute("""SELECT E.name,D.department_name
            FROM employee E JOIN departments D
            ON (E.department_id = D.departments_id)
            WHERE department_id=?
            """, (dept_id))
print(c.fetchall())
